package app

import (
	"github.com/revel/revel"
	"revel/app/models"
	"revel/app/models/database"
	"github.com/jinzhu/inflection"
	"fmt"
	"github.com/jinzhu/gorm"
	"reflect"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

func init() {
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,              // Resolve the requested language
		HeaderFilter,                  // Add some security based headers
		revel.InterceptorFilter,       // Run interceptors around the action.
		revel.CompressFilter,          // Compress the result.
		revel.ActionInvoker,           // Invoke the action.
	}

	// register startup functions with OnAppStart
	// revel.DevMode and revel.RunMode only work inside of OnAppStart. See Example Startup Script
	// ( order dependent )
	// revel.OnAppStart(ExampleStartupScript)
	revel.OnAppStart(database.InitDB)
	revel.OnAppStart(func() {
		InitializeDB()
	})
	// revel.OnAppStart(FillCache)
}

func InitializeDB() {
	database.DB.AutoMigrate(&models.Product{})
	//database.DB.AutoMigrate(&models.ProductHasAttributes{})
	database.DB.AutoMigrate(&models.ProductAttributesValues{})

	database.DB.AutoMigrate(&models.Category{})

	database.DB.AutoMigrate(&models.Attr{})
	database.DB.AutoMigrate(&models.AttrValues{})
	database.DB.AutoMigrate(&models.AttrHasAttrValues{})
	database.DB.AutoMigrate(&models.AttrSet{})
	database.DB.AutoMigrate(&models.AttrType{})

	database.DB.AutoMigrate(&models.Supplier{})
	database.DB.AutoMigrate(&models.SupplierHasProducts{})

	database.DB.AutoMigrate(&models.Warehouse{})
	database.DB.AutoMigrate(&models.WarehouseHasProducts{})

	database.DB.AutoMigrate(&models.Import{})
	database.DB.AutoMigrate(&models.Mapping{})

	Many2ManyFIndex(database.DB, &models.Attr{}, &models.AttrValues{})
}

func Many2ManyFIndex(db *gorm.DB, parentModel interface{}, childModel interface{}) {
	table1Accessor := ReduceModelToName(parentModel)
	table2Accessor := ReduceModelToName(childModel)

	table1Name := inflection.Plural(table1Accessor)
	table2Name := inflection.Plural(table2Accessor)

	joinTable := fmt.Sprintf("%s_has_%s", table1Accessor, table2Name)

	db.Table(joinTable).AddForeignKey(table1Accessor+"_id", table1Name+"(id)", "CASCADE", "CASCADE")
	db.Table(joinTable).AddForeignKey(table2Accessor+"_id", table2Name+"(id)", "CASCADE", "CASCADE")
	db.Table(joinTable).AddUniqueIndex(joinTable+"_unique", table1Accessor+"_id", table2Accessor+"_id")
}

func ReduceModelToName(model interface{}) string {
	value := reflect.ValueOf(model)
	if value.Kind() != reflect.Ptr {
		return ""
	}

	elem := value.Elem()
	t := elem.Type()
	rawName := t.Name()
	return gorm.ToDBName(rawName)
}

// HeaderFilter adds common security headers
// TODO turn this into revel.HeaderFilter
// should probably also have a filter for CSRF
// not sure if it can go in the same filter or not
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

//func ExampleStartupScript() {
//	// revel.DevMod and revel.RunMode work here
//	// Use this script to check for dev mode and set dev/prod startup scripts here!
//	if revel.DevMode == true {
//		// Dev mode
//	}
//}
