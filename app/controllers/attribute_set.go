package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type AttributeSet struct {
	*revel.Controller
}

func (c AttributeSet) Index() revel.Result {
	var attributeset models.AttrSet

	c.Response.Status = 200
	return c.Render(attributeset)
}

func (c AttributeSet) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, attributesets, err := models.AttrSet{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = attributesets
	results["count"] = count

	return c.RenderJSON(results)
}

func (c AttributeSet) Create() revel.Result {
	var (
		err          error
		attributeset models.AttrSet
	)
	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&attributeset, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	attributeset.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(AttributeSet.New)
	}

	attributeset, err = attributeset.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Attribute Set %d is successfully created!", attributeset.ID))
	return c.Redirect("/attribute-set/%d", attributeset.ID)
}

func (c AttributeSet) Update() revel.Result {
	var (
		err          error
		attributeset models.AttrSet
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&attributeset, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	attributeset.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/attribute-set/%d", attributeset.ID)
	}

	attributeset, err = attributeset.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Attribute Set %d is successfully updated!", attributeset.ID))
	return c.Redirect("/attribute-set/%d", attributeset.ID)
}

func (c AttributeSet) GridDelete(id string) revel.Result {
	var (
		err          error
		attributeset models.AttrSet
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			attributeset, err = attributeset.GetOne(id)
			if err != nil {
				return c.NotFound("Attribute Set not found", err)
			}

			err = attributeset.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c AttributeSet) Delete(id string) revel.Result {
	var (
		err          error
		attributeset models.AttrSet
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributesetID := parseUintOrDefault(id, 0)
	if attributesetID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeset, err = attributeset.GetOne(attributesetID)
	if err != nil {
		return c.NotFound("Attribute Set not found", err)
	}

	err = attributeset.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Attribute Set %d is successfully deleted!", attributeset.ID))
	return c.Redirect(AttributeSet.Index)
}

func (c AttributeSet) New() revel.Result {
	c.Response.Status = 200
	var attributeset models.AttrSet
	c.Render(attributeset)
	return c.RenderTemplate("AttributeSet/Edit.html")
}

func (c AttributeSet) Edit(id string) revel.Result {

	var (
		err          error
		attributeset models.AttrSet
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributesetID := parseUintOrDefault(id, 0)
	if attributesetID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeset, err = attributeset.GetOne(attributesetID)
	if err != nil {
		return c.NotFound("Attribute Set not found", err)
	}

	c.Response.Status = 200
	return c.Render(attributeset)
}
