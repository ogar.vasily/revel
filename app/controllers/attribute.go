package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type Attribute struct {
	*revel.Controller
}

func (c Attribute) Index() revel.Result {
	var attributes models.Attr

	c.Response.Status = 200
	return c.Render(attributes)
}

func (c Attribute) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, attributes, err := models.Attr{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = attributes
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Attribute) Create() revel.Result {
	var (
		err       error
		attribute models.Attr
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&attribute, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	attribute.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Attribute.New)
	}

	attribute, err = attribute.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Attribute %d is successfully created!", attribute.ID))
	return c.Redirect("/attribute/%d", attribute.ID)
}

func (c Attribute) Update() revel.Result {
	var (
		err       error
		attribute models.Attr
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&attribute, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	attribute.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/attribute/%d", attribute.ID)
	}

	attribute, err = attribute.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Attribute %d is successfully updated!", attribute.ID))
	return c.Redirect("/attribute/%d", attribute.ID)
}

func (c Attribute) Delete(id string) revel.Result {
	var (
		err       error
		attribute models.Attr
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeID := parseUintOrDefault(id, 0)
	if attributeID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	attribute, err = attribute.GetOne(attributeID)
	if err != nil {
		return c.NotFound("Attribute not found", err)
	}

	err = attribute.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Attribute %d is successfully deleted!", attribute.ID))
	return c.Redirect(Attribute.Index)
}

func (c Attribute) GridDelete() revel.Result {
	var (
		err       error
		attribute models.Attr
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			attribute, err = attribute.GetOne(id)
			if err != nil {
				return c.NotFound("Attribute not found", err)
			}

			err = attribute.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Attribute) New() revel.Result {
	c.Response.Status = 200
	var attribute models.Attr

	attributeSets, _ := models.AttrSet{}.GetAll()
	types, _ := models.AttrType{}.GetAll()

	c.Render(attribute, attributeSets, types)
	return c.RenderTemplate("Attribute/Edit.html")
}

func (c Attribute) Edit(id string) revel.Result {

	var (
		err       error
		attribute models.Attr
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeID := parseUintOrDefault(id, 0)
	if attributeID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	attribute, err = attribute.GetOne(attributeID)
	if err != nil {
		return c.NotFound("Attribute not found", err)
	}

	c.Response.Status = 200
	attributeSets, _ := models.AttrSet{}.GetAll()
	types, _ := models.AttrType{}.GetAll()

	fmt.Println(attribute.Values)
	return c.Render(attribute, attributeSets, types, nil)
}
