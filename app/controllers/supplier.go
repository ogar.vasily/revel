package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type Supplier struct {
	*revel.Controller
}

func (c Supplier) Index() revel.Result {
	var suppliers models.Supplier

	c.Response.Status = 200
	return c.Render(suppliers)
}

func (c Supplier) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, suppliers, err := models.Supplier{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = suppliers
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Supplier) ProductsGrid(id string) revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierID := parseUintOrDefault(id, 0)
	if supplierID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}
	supplierHasProducts := models.SupplierHasProducts{}
	supplierHasProducts.SupplierID = uint(supplierID)

	count, suppliers, err := supplierHasProducts.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = suppliers
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Supplier) Create() revel.Result {
	var (
		err      error
		supplier models.Supplier
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&supplier, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	supplier.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Supplier.New)
	}

	supplier, err = supplier.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Supplier %d is successfully created!", supplier.ID))
	return c.Redirect("/supplier/%d", supplier.ID)
}

func (c Supplier) Update() revel.Result {
	var (
		err      error
		supplier models.Supplier
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&supplier, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	supplier.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/supplier/%d", supplier.ID)
	}

	supplier, err = supplier.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Supplier %d is successfully updated!", supplier.ID))
	return c.Redirect("/supplier/%d", supplier.ID)
}

func (c Supplier) Delete(id string) revel.Result {
	var (
		err      error
		supplier models.Supplier
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierID := parseUintOrDefault(id, 0)
	if supplierID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplier, err = supplier.GetOne(supplierID)
	if err != nil {
		return c.NotFound("Supplier not found", err)
	}

	err = supplier.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Supplier %d is successfully deleted!", supplier.ID))
	return c.Redirect(Supplier.Index)
}

func (c Supplier) GridDelete() revel.Result {
	var (
		err      error
		supplier models.Supplier
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			supplier, err = supplier.GetOne(id)
			if err != nil {
				return c.NotFound("Supplier not found", err)
			}

			err = supplier.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Supplier) New() revel.Result {
	c.Response.Status = 200
	var supplier models.Supplier
	c.Render(supplier)
	return c.RenderTemplate("Supplier/Edit.html")
}

func (c Supplier) Edit(id string) revel.Result {

	var (
		err      error
		supplier models.Supplier
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierID := parseUintOrDefault(id, 0)
	if supplierID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplier, err = supplier.GetOne(supplierID)
	if err != nil {
		return c.NotFound("Supplier not found", err)
	}

	c.Response.Status = 200
	return c.Render(supplier)
}
