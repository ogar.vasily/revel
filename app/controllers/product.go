package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
	"time"
)

type Product struct {
	*revel.Controller
}

func (c Product) Index() revel.Result {
	var products models.Product

	c.Response.Status = 200
	return c.Render(products)
}

func (c Product) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, products, err := models.Product{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = products
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Product) Create() revel.Result {
	var (
		err     error
		product models.Product
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
		return c.Redirect(Product.New)
	}
	decoder := schema.NewDecoder()
	decoder.RegisterConverter(time.Time{}, models.ConvertFormDate)
	err = decoder.Decode(&product, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
		return c.Redirect(Product.New)
	}

	product.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Product.New)
	}

	product, err = product.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Product %d is successfully created!", product.ID))
	return c.Redirect("/product/%d", product.ID)
}

func (c Product) Update() revel.Result {
	var (
		err     error
		product models.Product
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	fmt.Println(c.Request.PostForm)
	decoder := schema.NewDecoder()
	decoder.RegisterConverter(time.Time{}, models.ConvertFormDate)
	err = decoder.Decode(&product, c.Request.PostForm)
	fmt.Println(product)

	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	product.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/product/%d", product.ID)
	}

	product, err = product.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Product %d is successfully updated!", product.ID))
	return c.Redirect("/product/%d", product.ID)
}

func (c Product) Delete(id string) revel.Result {
	var (
		err     error
		product models.Product
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	productID := parseUintOrDefault(id, 0)
	if productID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	product, err = product.GetOne(productID)
	if err != nil {
		return c.NotFound("Product not found", err)
	}

	err = product.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Product %d is successfully deleted!", product.ID))
	return c.Redirect(Product.Index)
}

func (c Product) GridDelete() revel.Result {
	var (
		err     error
		product models.Product
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			product, err = product.GetOne(id)
			if err != nil {
				return c.NotFound("Product not found", err)
			}

			err = product.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Product) New() revel.Result {
	c.Response.Status = 200
	var product models.Product
	categories, _ := models.Category{}.GetAll()
	attributes, _ := models.Attr{}.GetAll()
	var attrs []map[string]interface{}

	for _, a := range attributes {
		attr := make(map[string]interface{})
		attr["ID"] = a.ID
		attr["Name"] = a.Name
		attr["Code"] = a.Code
		attr["AttributeTypeID"] = a.AttributeTypeID
		attr["Values"] = a.Values
		attr["AttributeValuesID"] = ""
		attr["AttrValues"] = ""
		attr["Text"] = ""
		attr["Number"] = ""
		attr["Varchar"] = ""
		attr["Boolean"] = ""
		attr["DateTime"] = ""
		for _, v := range product.Values {
			fmt.Println(v.AttributeValuesID)
			if a.ID == v.AttributesID {
				attr["AttributeValuesID"] = v.ID
				attr["AttrValues"] = v.AttributeValuesID
				attr["Text"] = v.Text
				attr["Number"] = v.Number
				attr["Varchar"] = v.VarChar
				attr["Boolean"] = v.Boolean
				attr["DateTime"] = v.DateTime
			}
		}
		attrs = append(attrs, attr)
	}
	//fmt.Println(attrs)

	c.Render(product, categories, attrs)
	return c.RenderTemplate("Product/Edit.html")
}

func (c Product) Edit(id string) revel.Result {

	var (
		err     error
		product models.Product
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	productID := parseUintOrDefault(id, 0)
	if productID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	product, err = product.GetOne(productID)
	if err != nil {
		return c.NotFound("Product not found", err)
	}

	c.Response.Status = 200
	categories, _ := models.Category{}.GetAll()
	attributes, _ := models.Attr{}.GetAll()
	suppliers, _ := models.Supplier{}.GetAll()
	supplierHasProducts, _ := models.SupplierHasProducts{}.FindByProductId(productID)

	var sups []map[string]interface{}
	for _, s := range suppliers {
		attr := make(map[string]interface{})
		attr["id"] = s.ID
		attr["name"] = s.Name
		attr["price"] = ""
		attr["qty"] = ""
		attr["active"] = 0
		for _, p := range supplierHasProducts {
			if p.SupplierID == s.ID {
				attr["price"] = p.Price
				attr["qty"] = p.Qty
				attr["active"] = 1
			}
		}
		sups = append(sups, attr)
	}

	warehouseHasProducts, _ := models.WarehouseHasProducts{}.FindByProductId(productID)
	warehouses, _ := models.Warehouse{}.GetAll()
	var wareh []map[string]interface{}
	for _, w := range warehouses {
		attr := make(map[string]interface{})
		attr["id"] = w.ID
		attr["name"] = w.Name
		attr["price"] = ""
		attr["qty"] = ""
		attr["active"] = 0
		for _, p := range warehouseHasProducts {
			if p.WarehouseID == w.ID {
				attr["price"] = p.Price
				attr["qty"] = p.Qty
				attr["active"] = 1
			}
		}
		wareh = append(wareh, attr)
	}

	var attrs []map[string]interface{}

	for _, a := range attributes {
		attr := make(map[string]interface{})
		attr["ID"] = a.ID
		attr["Name"] = a.Name
		attr["Code"] = a.Code
		attr["AttributeTypeID"] = a.AttributeTypeID
		attr["Values"] = a.Values
		attr["AttributeValuesID"] = ""
		attr["AttrValues"] = ""
		attr["Text"] = ""
		attr["Number"] = ""
		attr["Varchar"] = ""
		attr["Boolean"] = ""
		attr["DateTime"] = ""
		for _, v := range product.Values {
			if a.ID == v.AttributesID {
				attr["AttributeValuesID"] = v.ID
				attr["AttrValues"] = v.AttributeValuesID
				attr["Text"] = v.Text
				attr["Number"] = v.Number
				attr["Varchar"] = v.VarChar
				attr["Boolean"] = v.Boolean
				attr["DateTime"] = v.DateTime
			}
		}
		attrs = append(attrs, attr)
	}

	return c.Render(product, categories, attrs, sups, wareh)
}
