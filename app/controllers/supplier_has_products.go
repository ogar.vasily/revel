package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type SupplierHasProducts struct {
	*revel.Controller
}

func (c SupplierHasProducts) Index() revel.Result {
	var supplierhasproductss models.SupplierHasProducts

	c.Response.Status = 200
	return c.Render(supplierhasproductss)
}

func (c SupplierHasProducts) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, supplierhasproductss, err := models.SupplierHasProducts{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = supplierhasproductss
	results["count"] = count

	return c.RenderJSON(results)
}

func (c SupplierHasProducts) Create() revel.Result {
	var (
		err                 error
		supplierhasproducts models.SupplierHasProducts
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&supplierhasproducts, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	supplierhasproducts.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(SupplierHasProducts.New)
	}

	supplierhasproducts, err = supplierhasproducts.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("SupplierHasProducts %d is successfully created!", supplierhasproducts.ID))
	return c.Redirect("/supplierhasproducts/%d", supplierhasproducts.ID)
}

func (c SupplierHasProducts) Update() revel.Result {
	var (
		err                 error
		supplierhasproducts models.SupplierHasProducts
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&supplierhasproducts, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	supplierhasproducts.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/supplierhasproducts/%d", supplierhasproducts.ID)
	}

	supplierhasproducts, err = supplierhasproducts.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("SupplierHasProducts %d is successfully updated!", supplierhasproducts.ID))
	return c.Redirect("/supplierhasproducts/%d", supplierhasproducts.ID)
}

func (c SupplierHasProducts) Delete(id string) revel.Result {
	var (
		err                 error
		supplierhasproducts models.SupplierHasProducts
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierhasproductsID := parseUintOrDefault(id, 0)
	if supplierhasproductsID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierhasproducts, err = supplierhasproducts.GetOne(supplierhasproductsID)
	if err != nil {
		return c.NotFound("SupplierHasProducts not found", err)
	}

	err = supplierhasproducts.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("SupplierHasProducts %d is successfully deleted!", supplierhasproducts.ID))
	return c.Redirect(SupplierHasProducts.Index)
}

func (c SupplierHasProducts) GridDelete() revel.Result {
	var (
		err                 error
		supplierhasproducts models.SupplierHasProducts
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			supplierhasproducts, err = supplierhasproducts.GetOne(id)
			if err != nil {
				return c.NotFound("SupplierHasProducts not found", err)
			}

			err = supplierhasproducts.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c SupplierHasProducts) New() revel.Result {
	c.Response.Status = 200
	var supplierhasproducts models.SupplierHasProducts
	c.Render(supplierhasproducts)
	return c.RenderTemplate("SupplierHasProducts/Edit.html")
}

func (c SupplierHasProducts) Edit(id string) revel.Result {

	var (
		err                 error
		supplierhasproducts models.SupplierHasProducts
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierhasproductsID := parseUintOrDefault(id, 0)
	if supplierhasproductsID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	supplierhasproducts, err = supplierhasproducts.GetOne(supplierhasproductsID)
	if err != nil {
		return c.NotFound("SupplierHasProducts not found", err)
	}

	c.Response.Status = 200
	return c.Render(supplierhasproducts)
}
