package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
	"github.com/gorilla/schema"
)

type Category struct {
	*revel.Controller
}

func (c Category) Index() revel.Result {
	var categorys models.Category

	c.Response.Status = 200
	return c.Render(categorys)
}

func (c Category) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, categorys, err := models.Category{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = categorys
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Category) Create() revel.Result {
	var (
		err      error
		category models.Category
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&category, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	category.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Category.New)
	}

	category, err = category.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Category %d is successfully created!", category.ID))
	return c.Redirect("/category/%d", category.ID)
}

func (c Category) Update() revel.Result {
	var (
		err      error
		category models.Category
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&category, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	category.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/category/%d", category.ID)
	}

	category, err = category.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Category %d is successfully updated!", category.ID))
	return c.Redirect("/category/%d", category.ID)
}

func (c Category) Delete(id string) revel.Result {
	var (
		err      error
		category models.Category
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	categoryID := parseUintOrDefault(id, 0)
	if categoryID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	category, err = category.GetOne(categoryID)
	if err != nil {
		return c.NotFound("Category not found", err)
	}

	err = category.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Category %d is successfully deleted!", category.ID))
	return c.Redirect(Category.Index)
}

func (c Category) GridDelete() revel.Result {
	var (
		err      error
		category models.Category
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			category, err = category.GetOne(id)
			if err != nil {
				return c.NotFound("Category not found", err)
			}

			err = category.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Category) New() revel.Result {
	c.Response.Status = 200
	var category models.Category
	c.Render(category)
	return c.RenderTemplate("Category/Edit.html")
}

func (c Category) Edit(id string) revel.Result {

	var (
		err      error
		category models.Category
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	categoryID := parseUintOrDefault(id, 0)
	if categoryID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	category, err = category.GetOne(categoryID)
	if err != nil {
		return c.NotFound("Category not found", err)
	}

	c.Response.Status = 200
	return c.Render(category)
}
