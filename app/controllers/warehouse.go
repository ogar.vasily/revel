package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type Warehouse struct {
	*revel.Controller
}

func (c Warehouse) Index() revel.Result {
	var warehouses models.Warehouse

	c.Response.Status = 200
	return c.Render(warehouses)
}

func (c Warehouse) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, warehouses, err := models.Warehouse{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = warehouses
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Warehouse) ProductsGrid(id string) revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehouseID := parseUintOrDefault(id, 0)
	if warehouseID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}
	warehouseHasProducts := models.WarehouseHasProducts{}
	warehouseHasProducts.WarehouseID = uint(warehouseID)

	count, warehouses, err := warehouseHasProducts.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = warehouses
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Warehouse) Create() revel.Result {
	var (
		err       error
		warehouse models.Warehouse
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&warehouse, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	warehouse.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Warehouse.New)
	}

	warehouse, err = warehouse.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Warehouse %d is successfully created!", warehouse.ID))
	return c.Redirect("/warehouse/%d", warehouse.ID)
}

func (c Warehouse) Update() revel.Result {
	var (
		err       error
		warehouse models.Warehouse
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&warehouse, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	warehouse.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/warehouse/%d", warehouse.ID)
	}

	warehouse, err = warehouse.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Warehouse %d is successfully updated!", warehouse.ID))
	return c.Redirect("/warehouse/%d", warehouse.ID)
}

func (c Warehouse) Delete(id string) revel.Result {
	var (
		err       error
		warehouse models.Warehouse
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehouseID := parseUintOrDefault(id, 0)
	if warehouseID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehouse, err = warehouse.GetOne(warehouseID)
	if err != nil {
		return c.NotFound("Warehouse not found", err)
	}

	err = warehouse.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Warehouse %d is successfully deleted!", warehouse.ID))
	return c.Redirect(Warehouse.Index)
}

func (c Warehouse) GridDelete() revel.Result {
	var (
		err       error
		warehouse models.Warehouse
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			warehouse, err = warehouse.GetOne(id)
			if err != nil {
				return c.NotFound("Warehouse not found", err)
			}

			err = warehouse.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Warehouse) New() revel.Result {
	c.Response.Status = 200
	var warehouse models.Warehouse
	c.Render(warehouse)
	return c.RenderTemplate("Warehouse/Edit.html")
}

func (c Warehouse) Edit(id string) revel.Result {

	var (
		err       error
		warehouse models.Warehouse
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehouseID := parseUintOrDefault(id, 0)
	if warehouseID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehouse, err = warehouse.GetOne(warehouseID)
	if err != nil {
		return c.NotFound("Warehouse not found", err)
	}

	c.Response.Status = 200
	return c.Render(warehouse)
}
