package controllers

import (
	"encoding/csv"
	"fmt"
	"github.com/revel/revel"
	"io"
	"log"
	"os"
	"path/filepath"
	"revel/app/models"
	"github.com/fatih/structs"
	"strings"
	"github.com/shopspring/decimal"
	"time"
	"strconv"
	"reflect"
	"github.com/pkg/errors"
	"regexp"
)

type CsvImport struct {
	*revel.Controller
}

var colToMap map[int]string
var imports models.Import

func (i CsvImport) Index(id string) revel.Result {
	var err error

	if id == "" {
		return i.Forbidden("Invalid id parameter", id)
	}

	importID := parseUintOrDefault(id, 0)
	if importID == 0 {
		return i.Forbidden("Invalid id parameter", id)
	}

	imports, err = models.Import{}.GetOne(importID)
	if err != nil {
		return i.Forbidden("Invalid import", err)
	}
	i.extract()

	return i.Render()
}

func (i CsvImport) extract() error {
	pwd, _ := os.Getwd()
	absPath, _ := filepath.Abs(pwd + "/public/files/" + imports.FileName)

	f, err := os.Open(absPath)
	if err != nil {
		log.Printf("File open error: %s", err)
		i.Flash.Error(fmt.Sprintf("File open error: %s", err))
	}
	defer f.Close()
	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true

	if err != nil {
		return err
	}

	index := 0
	//var header []string
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			log.Printf("File error: %s", err)
			break
		} else if err != nil {
			log.Printf("File error: %s", err)
			i.Flash.Error(fmt.Sprintf("File error: %s", err))
		}

		if index == (imports.HeaderRow - 1) {
			colToMap = imports.ColToMapping(row)
		}

		if index > (imports.HeaderRow - 1) {
			//fmt.Println("Index: ", index)
			product, err := i.transform(row, imports)
			if err != nil {
				panic(err)
			}
			if product != nil {
				i.load(*product)
			}
		}

		index++
	}
	return nil
}

func (i CsvImport) modification(in, mod decimal.Decimal, modType, modMethod string) (decimal.Decimal, error) {
	var modifier, modified decimal.Decimal
	if modType == "per_cent" {
		hundred, _ := decimal.NewFromString("100")
		switch modMethod {
		case "add":
			modifier := (mod.Add(hundred)).Div(hundred) // +20% (20 + 100)/100 = 1.2
			modified = in.Mul(modifier)                 // 		10 * 1.2 = 12
		case "sub":
			modifier = mod.Div(hundred)         // -20%  20/100 = 0.2
			modified = in.Sub(in.Mul(modifier)) // 		 10 - (10 * 0.2) = 8
		case "mul":
			modifier = mod.Div(hundred) // *20%  20/100 = 0.2
			modified = in.Mul(modifier) // 		 10 * 0.2 = 2
		case "div":
			modifier = mod.Div(hundred) // /20%  20/100 = 0.2
			modified = in.Div(modifier) //  	 10 / 0.2 = 50
		default:
			return decimal.Zero, errors.New("Wrong modification method")
		}
	} else if modType == "fixed" {
		switch modMethod {
		case "add":
			modified = in.Add(mod)
		case "sub":
			modified = in.Sub(mod)
		case "mul":
			modified = in.Mul(mod)
		case "div":
			modified = in.Div(mod)
		default:
			return decimal.Zero, errors.New("Wrong modification method")
		}
	} else {
		return decimal.Zero, errors.New("Modifier wasn't set up")
	}
	//fmt.Println(in)
	//fmt.Println(mod)
	//fmt.Println(modType)
	//fmt.Println(modMethod)
	//fmt.Println(modified)

	return modified, nil
}

func (i CsvImport) transform(row []string, imports models.Import) (*models.Product, error) {
	product := new(models.Product)
	s := structs.New(product)
	names := s.Names()
	supplierHasProducts := models.SupplierHasProducts{}
	warehouseHasProducts := models.WarehouseHasProducts{}
	supplierHasProducts.SupplierID = imports.SupplierID
	warehouseHasProducts.WarehouseID = imports.WarehouseID
	multivalueAttrs := []models.AttrValues{}

	//fmt.Printf("%+v\n", colToMap)
	//fmt.Printf("%+v\n", imports)
	for k, v := range colToMap {
		//fmt.Println("Row key: " + row[k])
		//fmt.Println("Row value: " + v)
		switch {
		case v == "price":
			r := regexp.MustCompile(`.*?(\d+\.?\d+).*`)
			pr := r.ReplaceAllString(row[k], `$1`)
			fmt.Println("Price: " + pr)
			price, _ := decimal.NewFromString(pr)
			supplierHasProducts.Price = price

			modifiedPrice, err := i.modification(price, imports.PriceModifier, imports.PriceModType, imports.PriceModMethod)
			if err != nil {
				return nil, err
			}
			warehouseHasProducts.Price = modifiedPrice
		case v == "sku":
			sku := row[k]
			supplierHasProducts.Sku = sku
			// Temporary we didn't set any sku to a warehouse
			//warehouseHasProducts.Sku = sku
		case v == "qty":
			var q string
			out := []string{"No UPC", "#N/A"}
			in := []string{"In stock"}
			if in_array(row[k], out) {
				q = "0"
			} else if in_array(row[k], in) {
				q = "1"
			} else {
				tmp, _ := decimal.NewFromString(row[k])
				zero, _ := decimal.NewFromString("0")
				if tmp.LessThanOrEqual(zero) {
					q = "0"
				} else {
					q = row[k]
				}
			}
			//fmt.Println("Qty: " + q)

			qty, _ := decimal.NewFromString(q)
			supplierHasProducts.Qty = qty

			modifiedQty, err := i.modification(qty, imports.QtyLimit, imports.QtyModType, imports.QtyModMethod)
			if err != nil {
				return nil, err
			}

			warehouseHasProducts.Qty = modifiedQty.Floor()
		case in_array(strings.Title(v), names):
			for _, name := range names {
				if strings.ToLower(name) == v {
					field := s.Field(name)
					field.Set(row[k])
				}
			}
		default:
			attr, err := models.Attr{}.GetOneByCode(v, false)
			if err != nil {
				return nil, err
			}
			// Attribute type VarChar
			if attr.AttributeTypeID == 1 {
				productAttr := models.ProductAttributesValues{}
				productAttr.VarChar = row[k]
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}

			// Attribute type Text
			if attr.AttributeTypeID == 2 {
				productAttr := models.ProductAttributesValues{}
				productAttr.Text = row[k]
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}

			// Attribute type Number
			if attr.AttributeTypeID == 4 {
				productAttr := models.ProductAttributesValues{}
				productAttr.Number, _ = decimal.NewFromString(row[k])
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}

			// Attribute type Date
			if attr.AttributeTypeID == 10 {
				productAttr := models.ProductAttributesValues{}
				productAttr.DateTime, _ = time.Parse("2017-09-17 17:36:39", row[k])
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}

			// Attribute type Checkbox
			if attr.AttributeTypeID == 11 {
				productAttr := models.ProductAttributesValues{}
				int, _ := strconv.Atoi(row[k])
				productAttr.Boolean = uint8(int)
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}

			// Attribute type select or multiselect
			if attr.AttributeTypeID == 3 || attr.AttributeTypeID == 9 {
				attrValue := models.AttrValues{}
				attrValue.Value = row[k]
				attrValue.Attributes = append(attrValue.Attributes, attr)
				attrValue, err := attrValue.AddOrGet()
				if err != nil {
					i.Flash.Error(fmt.Sprintf("Can't add attribute value: %s, error %s", row[k], err))
				}
				multivalueAttrs = append(multivalueAttrs, attrValue)

				productAttr := models.ProductAttributesValues{}
				productAttr.AttributeValuesID = attrValue.ID
				productAttr.AttributesID = attr.ID
				product.Values = append(product.Values, productAttr)
			}
			//fmt.Printf("Attribute: %+v\n", attr)
		}
	}

	product.SupplierHasProducts = append(product.SupplierHasProducts, supplierHasProducts)
	product.WarehouseHasProducts = append(product.WarehouseHasProducts, warehouseHasProducts)
	multivalueCreated := i.multivalueCreated(multivalueAttrs)

	//fmt.Println(len(multivalueAttrs))
	//fmt.Println(multivalueCreated)

	if len(multivalueAttrs) == 0 || multivalueCreated {

		fmt.Printf("Product: %+v\n", product)
		return product, nil
	}

	return nil, nil
}

func (i CsvImport) multivalueCreated(multivalueAttrs []models.AttrValues) bool {
	valid := false
	tmp := 0
	for _, value := range multivalueAttrs {
		//fmt.Printf("AttributeValue: %+v\n", value)
		if value.ID != 0 {
			tmp++
		}
	}
	//fmt.Printf("AttrValues: %+v\n", multivalueAttrs)
	//fmt.Println(len(multivalueAttrs))
	//fmt.Println(tmp)
	if tmp == len(multivalueAttrs) {
		valid = true
	}
	return valid
}

func (i CsvImport) load(product models.Product) {
	var err error

	where := models.Product{}
	where.Ean = product.Ean

	if imports.UpdateType == "delete" {
		fmt.Println("delete")
		err = product.Delete()
	} else if imports.UpdateType == "update" {
		fmt.Println("update")
		p, _ := product.GetOneBy(where)
		product.ID = p.ID
		fmt.Printf("Found Product: %+v\n", p)
		_, err = product.Update()
	} else if imports.UpdateType == "new" {
		fmt.Println("new")
		_, err = product.Add()
	} else {
		fmt.Println("upsert")
		_, err = product.Upsert(where)
	}

	if err != nil {
		fmt.Println(err)
	}
}

func headerToData(row []string, header map[string]string) map[string]string {
	response := map[string]string{}
	i := 0
	for _, value := range header {
		response[value] = row[i]
		i++
	}
	return response
}

func in_array(needle interface{}, array interface{}) bool {
	val := reflect.Indirect(reflect.ValueOf(array))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < val.Len(); i++ {
			if needle == val.Index(i).Interface() {
				return true
			}
		}
	}
	return false
}
