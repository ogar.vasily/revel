package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
)

type WarehouseHasProducts struct {
	*revel.Controller
}

func (c WarehouseHasProducts) Index() revel.Result {
	var warehousehasproductss models.WarehouseHasProducts

	c.Response.Status = 200
	return c.Render(warehousehasproductss)
}

func (c WarehouseHasProducts) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, warehousehasproductss, err := models.WarehouseHasProducts{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = warehousehasproductss
	results["count"] = count

	return c.RenderJSON(results)
}

func (c WarehouseHasProducts) Create() revel.Result {
	var (
		err                  error
		warehousehasproducts models.WarehouseHasProducts
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&warehousehasproducts, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	warehousehasproducts.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(WarehouseHasProducts.New)
	}

	warehousehasproducts, err = warehousehasproducts.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("WarehouseHasProducts %d is successfully created!", warehousehasproducts.ID))
	return c.Redirect("/warehousehasproducts/%d", warehousehasproducts.ID)
}

func (c WarehouseHasProducts) Update() revel.Result {
	var (
		err                  error
		warehousehasproducts models.WarehouseHasProducts
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&warehousehasproducts, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	warehousehasproducts.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/warehousehasproducts/%d", warehousehasproducts.ID)
	}

	warehousehasproducts, err = warehousehasproducts.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("WarehouseHasProducts %d is successfully updated!", warehousehasproducts.ID))
	return c.Redirect("/warehousehasproducts/%d", warehousehasproducts.ID)
}

func (c WarehouseHasProducts) Delete(id string) revel.Result {
	var (
		err                  error
		warehousehasproducts models.WarehouseHasProducts
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehousehasproductsID := parseUintOrDefault(id, 0)
	if warehousehasproductsID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehousehasproducts, err = warehousehasproducts.GetOne(warehousehasproductsID)
	if err != nil {
		return c.NotFound("WarehouseHasProducts not found", err)
	}

	err = warehousehasproducts.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("WarehouseHasProducts %d is successfully deleted!", warehousehasproducts.ID))
	return c.Redirect(WarehouseHasProducts.Index)
}

func (c WarehouseHasProducts) GridDelete() revel.Result {
	var (
		err                  error
		warehousehasproducts models.WarehouseHasProducts
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			warehousehasproducts, err = warehousehasproducts.GetOne(id)
			if err != nil {
				return c.NotFound("WarehouseHasProducts not found", err)
			}

			err = warehousehasproducts.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c WarehouseHasProducts) New() revel.Result {
	c.Response.Status = 200
	var warehousehasproducts models.WarehouseHasProducts
	c.Render(warehousehasproducts)
	return c.RenderTemplate("WarehouseHasProducts/Edit.html")
}

func (c WarehouseHasProducts) Edit(id string) revel.Result {

	var (
		err                  error
		warehousehasproducts models.WarehouseHasProducts
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehousehasproductsID := parseUintOrDefault(id, 0)
	if warehousehasproductsID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	warehousehasproducts, err = warehousehasproducts.GetOne(warehousehasproductsID)
	if err != nil {
		return c.NotFound("WarehouseHasProducts not found", err)
	}

	c.Response.Status = 200
	return c.Render(warehousehasproducts)
}
