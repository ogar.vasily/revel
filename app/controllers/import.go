package controllers

import (
	"fmt"
	"github.com/gorilla/schema"
	"github.com/revel/revel"
	"github.com/tidwall/gjson"
	"revel/app/models"
	"strings"
)

type Import struct {
	*revel.Controller
}

func (c Import) Index() revel.Result {
	var imports models.Import

	c.Response.Status = 200
	return c.Render(imports)
}

func (c Import) Grid() revel.Result {
	limit, offset, where, sort := GetGridParams(c.Params.JSON)

	count, imports, err := models.Import{}.GetMany(limit, offset, where, sort)
	if err != nil {
		return c.RenderError(err)
	}
	c.Response.Status = 200
	results := make(map[string]interface{})
	results["result"] = imports
	results["count"] = count

	return c.RenderJSON(results)
}

func (c Import) Create() revel.Result {
	var (
		err error
		is  models.Import
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(&is, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	is.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(Import.New)
	}

	is, err = is.Add()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Import %d is successfully created!", is.ID))
	return c.Redirect("/import/%d", is.ID)
}

func (c Import) Update() revel.Result {
	var (
		err error
		i   models.Import
	)

	err = c.Request.ParseForm()
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}
	post := c.Request.PostForm
	fmt.Println("postform")
	fmt.Println(post)
	fmt.Println("formValue")

	decoder := schema.NewDecoder()
	err = decoder.Decode(&i, c.Request.PostForm)
	if err != nil {
		c.Flash.Error(fmt.Sprintln("Error", err))
	}

	i.Validate(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/import/%d", i.ID)
	}

	i, err = i.Update()
	if err != nil {
		return c.RenderError(err)
	}

	c.Flash.Success(fmt.Sprintf("Import %d is successfully updated!", i.ID))
	return c.Redirect("/import/%d", i.ID)
}

func (c Import) Delete(id string) revel.Result {
	var (
		err error
		i   models.Import
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	importID := parseUintOrDefault(id, 0)
	if importID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	i, err = i.GetOne(importID)
	if err != nil {
		return c.NotFound("Import not found", err)
	}

	err = i.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("Import %d is successfully deleted!", i.ID))
	return c.Redirect(Import.Index)
}

func (c Import) GridDelete() revel.Result {
	var (
		err error
		i   models.Import
	)

	fmt.Println(c.Params.JSON)

	deleted := gjson.GetBytes(c.Params.JSON, "deleted")

	if deleted.Exists() {
		for _, v := range deleted.Array() {
			id := v.Get("ID").Uint()
			fmt.Println(id)
			if id == 0 {
				return c.Forbidden("Invalid id parameter", id)
			}

			i, err = i.GetOne(id)
			if err != nil {
				return c.NotFound("Import not found", err)
			}

			err = i.Delete()
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	return c.RenderJSON("Success")
}

func (c Import) New() revel.Result {
	c.Response.Status = 200
	var i models.Import
	attributes, _ := models.Attr{}.GetAll()
	var attrs []map[string]interface{}
	for _, a := range attributes {
		attr := make(map[string]interface{})
		attr["Code"] = a.Code
		attrs = append(attrs, attr)
	}
	fmt.Println(attrs)
	c.Render(i, attrs)
	return c.RenderTemplate("Import/Edit.html")
}

func (c Import) Edit(id string) revel.Result {

	var (
		err error
		i   models.Import
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	importID := parseUintOrDefault(id, 0)
	if importID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	i, err = i.GetOne(importID)
	if err != nil {
		return c.NotFound("Import not found", err)
	}
	attributes, _ := models.Attr{}.GetAll()
	var attrs []map[string]interface{}
	product_attr := []string{"name", "status", "description", "weight", "barcode", "ean", "upc", "sku", "price", "qty"}
	for _, a := range product_attr {
		attr := make(map[string]interface{})
		attr["Code"] = a
		attr["Name"] = strings.Title(a)
		attrs = append(attrs, attr)
	}

	for _, a := range attributes {
		attr := make(map[string]interface{})
		attr["Code"] = a.Code
		attr["Name"] = a.Name
		attrs = append(attrs, attr)
	}

	//fmt.Println(attrs)
	warehouses, _ := models.Warehouse{}.GetAll()
	suppliers, _ := models.Supplier{}.GetAll()

	c.Response.Status = 200
	return c.Render(i, attrs, warehouses, suppliers)
}

func (c Import) DeleteMapping(id string) revel.Result {
	var m models.Mapping

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	mappingID := parseUintOrDefault(id, 0)
	if mappingID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	err := m.Delete(mappingID)
	if err != nil {
		return c.RenderJSON(err)
	}
	return c.RenderJSON("ok")
}
