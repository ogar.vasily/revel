package controllers

import (
	"github.com/tidwall/gjson"
)

func GetGridParams(json []byte) (int, int, []map[string]string, []string) {
	skip := gjson.GetBytes(json, "skip")
	take := gjson.GetBytes(json, "take")
	sortQuery := gjson.GetBytes(json, "sorted")
	whereQuery := gjson.GetBytes(json, "where.0")

	offset := int(skip.Int())
	limit := int(take.Int())
	where := getWhere(whereQuery)
	sort := getSort(sortQuery)

	return offset, limit, where, sort
}

func getWhere(p gjson.Result) []map[string]string {
	var where []map[string]string

	if p.Get("isComplex").Bool() && !p.Get("value").Exists() {
		where = getPredicates(p.Get("predicates"))
	} else {
		w := getWhereFromQuery(p)
		where = append(where, w)
	}

	return where
}

func getPredicates(p gjson.Result) []map[string]string {
	var predicates []map[string]string

	for _, q := range p.Array() {
		if q.Get("predicates").Exists() {
			predicates = getPredicates(q.Get("predicates"))
		} else {
			where := getWhereFromQuery(q)
			predicates = append(predicates, where)
		}
	}

	return predicates
}

func getWhereFromQuery(q gjson.Result) map[string]string {
	query := q.Map()
	var w = map[string]string{}
	w["column"] = query["field"].String()
	w["value"] = query["value"].String()
	w["operator"] = query["operator"].String()
	w["condition"] = query["condition"].String()

	return w
}

func getSort(sorted gjson.Result) []string {
	var sort []string
	for _, q := range sorted.Array() {
		s := getSortFromQuery(q)
		sort = append(sort, s)
	}
	return sort
}

func getSortFromQuery(q gjson.Result) string {
	query := q.Map()
	s := query["name"].String() + " " + getDirection(query["direction"].String())
	return s
}

func getDirection(d string) string {
	direction := "asc"
	if d == "descending" {
		direction = "desc"
	}
	return direction
}
