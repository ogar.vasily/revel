package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"revel/app/models"
	"strconv"
)

type AttributeValues struct {
	*revel.Controller
}

func (c AttributeValues) Index() revel.Result {
	var values models.AttrValues

	c.Response.Status = 200
	return c.Render(values)
}

func (c AttributeValues) Create() revel.Result {
	var (
		err   error
		value models.AttrValues
	)

	//err = c.Request.ParseForm()
	//if err != nil {
	//	c.Flash.Error(fmt.Sprintln("Error", err))
	//}
	//decoder := schema.NewDecoder()
	//err = decoder.Decode(&value, c.Request.PostForm)
	//if err != nil {
	//	c.Flash.Error(fmt.Sprintln("Error", err))
	//}
	//
	//value.Validate(c.Validation)
	//if c.Validation.HasErrors() {
	//	c.Validation.Keep()
	//	c.FlashParams()
	//	//return c.Render(value)
	//}

	post := c.Request.PostForm
	fmt.Println(post)
	tmpId, _ := strconv.ParseUint(post.Get("AttributesID"), 10, 32)
	attributeID := uint64(tmpId)
	attribute, err := models.Attr{}.GetOne(attributeID)
	if err != nil {
		return c.NotFound("Attribute not found", err)
	}
	for _, v := range post["Value[][Value]"] {
		if v != "" {
			//tmp := value
			value.Value = v

			value.Attributes = append(value.Attributes, attribute)
			_, err = value.AddOrGet()
			//c.Flash.Success(fmt.Sprintf("AttrValues %d is successfully created!", value.ID))
			if err != nil {
				return c.RenderError(err)
			}
		}
	}

	//value, err = value.Add()
	//c.Flash.Success(fmt.Sprintf("AttrValues %d is successfully created!", value.ID))
	if err != nil {
		return c.RenderError(err)
	}

	return c.Redirect("/attribute/%s", post.Get("AttributesID"))
}

func (c AttributeValues) Update() revel.Result {
	var (
		err   error
	)

	//err = c.Request.ParseForm()
	//if err != nil {
	//	c.Flash.Error(fmt.Sprintln("Error", err))
	//}
	//decoder := schema.NewDecoder()
	//err = decoder.Decode(&value, c.Request.PostForm)
	//if err != nil {
	//	c.Flash.Error(fmt.Sprintln("Error", err))
	//}
	//
	//value.Validate(c.Validation)
	//if c.Validation.HasErrors() {
	//	c.Validation.Keep()
	//	c.FlashParams()
	//}

	post := c.Request.PostForm

	fmt.Printf("Post: %+v\n", post)
	tmpId, _ := strconv.ParseUint(post.Get("AttributesID"), 10, 32)
	attributeID := uint64(tmpId)
	attribute, err := models.Attr{}.GetOne(attributeID)
	for _, a := range attribute.Values {
		a.Delete()
	}
	for i, v := range post["Value[][Value]"] {
		tmp := models.AttrValues{}
		tmpId, _ := strconv.ParseUint(post["Value[][ID]"][i], 10, 32)
		if tmpId != 0 {
			tmp.ID = uint(tmpId)
		}
		tmp.Value = v
		tmp.Attributes = append(tmp.Attributes, attribute)
		fmt.Printf("Value: %+v\n", tmp)
		_, err = tmp.Update()
		if err != nil {
			return c.RenderError(err)
		}
		c.Flash.Success(fmt.Sprintf("AttrValues %d is successfully created!", tmp.ID))
	}
	fmt.Printf("AttributesID: %+v\n", post.Get("AttributesID"))
	c.Flash.Success(fmt.Sprintf("AttrValues %d is successfully updated!", attributeID))
	return c.Redirect("/attribute/%s", post.Get("AttributesID"))
}

func (c AttributeValues) Delete(id string) revel.Result {
	var (
		err   error
		value models.AttrValues
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeID := parseUintOrDefault(id, 0)
	if attributeID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	value, err = value.GetOne(attributeID)
	if err != nil {
		return c.NotFound("AttrValues not found", err)
	}

	err = value.Delete()
	if err != nil {
		return c.RenderError(err)
	}
	c.Flash.Success(fmt.Sprintf("AttrValues %d is successfully deleted!", value.ID))
	return c.Redirect(AttributeValues.Index)
}

func (c AttributeValues) New() revel.Result {
	c.Response.Status = 200
	var value models.AttrValues

	attributeSets, _ := models.AttrSet{}.GetAll()
	types, _ := models.AttrType{}.GetAll()

	c.Render(value, attributeSets, types)
	return c.Redirect(Attribute.Index)
}

func (c AttributeValues) Edit(id string) revel.Result {

	var (
		err   error
		value models.AttrValues
	)

	if id == "" {
		return c.Forbidden("Invalid id parameter", id)
	}

	attributeID := parseUintOrDefault(id, 0)
	if attributeID == 0 {
		return c.Forbidden("Invalid id parameter", id)
	}

	value, err = value.GetOne(attributeID)
	if err != nil {
		return c.NotFound("AttrValues not found", err)
	}

	c.Response.Status = 200
	attributeSets, _ := models.AttrSet{}.GetAll()
	types, _ := models.AttrType{}.GetAll()

	return c.Render(value, attributeSets, types)
}
