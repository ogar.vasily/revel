package models

import (
	"github.com/satori/go.uuid"
	"time"
)

type CategoryOrig struct {
	Model

	Name        string    `gorm:"size:255;not null"`
	Status      uint8     `gorm:"default:1;not null"`
	Order       uint      `gorm:"default: 0;not null"`
	Description string    `gorm:"size:65000"`
	ParentId    uuid.UUID `gorm:"type:varchar(36)"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
}
