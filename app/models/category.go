package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"revel/app/models/database"
)

type Category struct {
	gorm.Model
	Name        string `json:"name" gorm:"column:Name"`
	Status      uint8  `json:"status" gorm:"column:Status"`
	Order       uint   `json:"order" gorm:"column:Order"`
	Description string `json:"description" gorm:"column:Description"`
	ParentId    uint   `json:"parentid" gorm:"column:ParentId"`
}

func (m *Category) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Category) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Category) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddCategory insert a new Category into database and returns
// last inserted category on success.
func (m Category) Add() (Category, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateCategory update a Category into database and returns
// last nil on success.
func (m Category) Update() (Category, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteCategory Delete Category from database and returns
// last nil on success.
func (m Category) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetCategorys Get all Category from database and returns
// list of Category on success
func (m Category) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Category, error) {
	var (
		err       error
		categorys []Category
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&categorys).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&categorys).Error
	} else {
		err = tx.Find(&categorys).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&categorys).Error
	}

	if err != nil {
		tx.Rollback()
		return count, categorys, err
	}

	tx.Commit()

	return count, categorys, err
}

// GetCategory Get a Category from database and returns
// a Category on success
func (m Category) GetOne(id uint64) (Category, error) {
	var (
		category Category
		err      error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&category, id).Error; err != nil {
		tx.Rollback()
		return category, err
	}
	tx.Commit()
	return category, err
}

func (m Category) GetAll() ([]Category, error) {
	var (
		err        error
		categories []Category
	)
	tx := database.DB.Begin()
	if err = tx.Find(&categories).Error; err != nil {
		tx.Rollback()
		return categories, err
	}
	tx.Commit()
	return categories, err
}

func MigrateCategory() {
	database.DB.AutoMigrate(&Category{})
}

func (Category) TableName() string {
	return "categorys"
}

func (category *Category) Validate(v *revel.Validation) {
	//Validation rules here
}
