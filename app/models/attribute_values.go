package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
	"fmt"
)

type AttrValues struct {
	gorm.Model

	Value      string `schema: "Value[]"`
	Attributes []Attr
}

// AddAttribute insert a new AttrValues into database and returns
// last inserted value on success.
func (m AttrValues) Add() (AttrValues, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Set("gorm:save_associations", false).Create(&m).Association("Attributes").Replace(m.Attributes).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

func (m AttrValues) AddOrGet() (AttrValues, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}
	//w := AttrValues{}
	tx := database.DB.Begin()
	fmt.Println(m.Attributes)
	err = tx.Where("value = ?", m.Value).Assign(m).FirstOrCreate(&m).Error
	if err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateAttribute update a AttrValues into database and returns
// last nil on success.
func (m AttrValues) Update() (AttrValues, error) {
	var err error

	tx := database.DB.Begin()
	if m.Value != "" {
		w := AttrValues{}
		w.Value = m.Value
		if err = tx.Where("id = ?", m.ID).Assign(w.Value).FirstOrCreate(&w).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}
	if err = tx.Model(m).Association("Attributes").Replace(m.Attributes).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	//if err = tx.Set("gorm:save_associations", false).Save(&r).Error; err != nil {
	//	tx.Rollback()
	//	return m, err
	//}
	tx.Commit()
	return m, err
}

// DeleteAttribute Delete AttrValues from database and returns
// last nil on success.
func (m AttrValues) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Unscoped().Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetAttribute Get a AttrValues from database and returns
// a AttrValues on success
func (m AttrValues) GetOne(id uint64) (AttrValues, error) {
	var (
		value AttrValues
		err   error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&value, id).Error; err != nil {
		tx.Rollback()
		return value, err
	}
	tx.Commit()
	return value, err
}

func (m AttrValues) GetAll() ([]AttrValues, error) {
	var (
		err        error
		attributes []AttrValues
	)
	tx := database.DB.Begin()
	if err = tx.Find(&attributes).Error; err != nil {
		tx.Rollback()
		return attributes, err
	}
	tx.Commit()
	return attributes, err
}

func (m AttrValues) MigrateAttribute() {
	database.DB.AutoMigrate(&AttrValues{})
}

func (a *AttrValues) Validate(v *revel.Validation) {

	//v.Check(a.Value, revel.ValidRequired()).Message("Value is required.")
}
