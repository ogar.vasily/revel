package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
)

type Warehouse struct {
	gorm.Model
	Name          string `json:"name" gorm:"column:Name"`
	VatNumber     string `json:"vatnumber" gorm:"column:VatNumber"`
	CompanyNumber string `json:"companynumber" gorm:"column:CompanyNumber"`
}

func (m *Warehouse) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Warehouse) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Warehouse) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddWarehouse insert a new Warehouse into database and returns
// last inserted warehouse on success.
func (m Warehouse) Add() (Warehouse, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateWarehouse update a Warehouse into database and returns
// last nil on success.
func (m Warehouse) Update() (Warehouse, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteWarehouse Delete Warehouse from database and returns
// last nil on success.
func (m Warehouse) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetWarehouses Get all Warehouse from database and returns
// list of Warehouse on success
func (m Warehouse) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Warehouse, error) {
	var (
		err        error
		warehouses []Warehouse
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&warehouses).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&warehouses).Error
	} else {
		err = tx.Find(&warehouses).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&warehouses).Error
	}

	if err != nil {
		tx.Rollback()
		return count, warehouses, err
	}

	tx.Commit()

	return count, warehouses, err
}

// GetWarehouse Get a Warehouse from database and returns
// a Warehouse on success
func (m Warehouse) GetOne(id uint64) (Warehouse, error) {
	var (
		warehouse Warehouse
		err       error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&warehouse, id).Error; err != nil {
		tx.Rollback()
		return warehouse, err
	}
	tx.Commit()
	return warehouse, err
}

func (m Warehouse) GetAll() ([]Warehouse, error) {
	var (
		err        error
		warehouses []Warehouse
	)
	tx := database.DB.Begin()
	if err = tx.Find(&warehouses).Error; err != nil {
		tx.Rollback()
		return warehouses, err
	}
	tx.Commit()
	return warehouses, err
}

func MigrateWarehouse() {
	database.DB.AutoMigrate(&Warehouse{})
}

func (Warehouse) TableName() string {
	return "warehouses"
}

func (warehouse *Warehouse) Validate(v *revel.Validation) {
	//Validation rules here
}
