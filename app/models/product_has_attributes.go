package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
)

type ProductHasAttributes struct {
	gorm.JoinTableHandler
	ID          uint      `gorm:"primary_key"`
	Product     []Product `gorm:"ForeignKey:ID;AssociationForeignKey:ProductID"`
	ProductID   uint
	Attribute   []Attr    `gorm:"ForeignKey:ID;AssociationForeignKey:AttrID"`
	AttributeID uint
}

func (*ProductHasAttributes) Add(handler gorm.JoinTableHandlerInterface, db *gorm.DB, foreignValue interface{}, associationValue interface{}) error {
	foreignPrimaryKey, _ := strconv.ParseUint(fmt.Sprint(db.NewScope(foreignValue).PrimaryKeyValue()), 10, 32)
	associationPrimaryKey, _ := strconv.ParseUint(fmt.Sprint(db.NewScope(associationValue).PrimaryKeyValue()), 10, 32)
	if result := db.Unscoped().Model(&ProductHasAttributes{}).Where(map[string]interface{}{
		"product_id":   foreignPrimaryKey,
		"attribute_id": associationPrimaryKey,
	}).Update(map[string]interface{}{
		"product_id":   uint(foreignPrimaryKey),
		"attribute_id": uint(associationPrimaryKey),
	}).RowsAffected; result == 0 {
		return db.Create(&ProductHasAttributes{
			ProductID:   uint(foreignPrimaryKey),
			AttributeID: uint(associationPrimaryKey),
		}).Error
	}

	return nil
}

func (*ProductHasAttributes) Delete(handler gorm.JoinTableHandlerInterface, db *gorm.DB, sources ...interface{}) error {
	return db.Delete(&ProductHasAttributes{}).Error
}

func (pa *ProductHasAttributes) JoinWith(handler gorm.JoinTableHandlerInterface, db *gorm.DB, source interface{}) *gorm.DB {
	return db.Joins("INNER JOIN product_has_attributes AS T1 ON T1.attribute_id = attributes.id")
}
