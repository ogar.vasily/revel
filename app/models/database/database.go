package database

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"github.com/robfig/config"
	"path"
	"errors"
)

var (
	DB *gorm.DB
)

func InitDB() {
	var (
		err      error
		driver   string
		database string
		port     string
		host     string
		username string
		password string
	)

	//load config
	configPath := path.Join(revel.BasePath, "conf", "database.conf")
	config, err := config.ReadDefault(configPath)
	if err != nil || config == nil {
		panic(err)
	}

	switch revel.RunMode {
	case "dev":
		driver, _ = config.String("dev", "driver");
		database, _ = config.String("dev", "database");
		port, _ = config.String("dev", "port");
		username, _ = config.String("dev", "username");
		password, _ = config.String("dev", "password");
		host, _ = config.String("dev", "host");
	case "prod":
		driver, _ = config.String("prod", "driver");
		database, _ = config.String("prod", "database");
		port, _ = config.String("prod", "port");
		username, _ = config.String("prod", "username");
		password, _ = config.String("prod", "password");
		host, _ = config.String("prod", "host");
	default:
		panic(errors.New("Invalid RunMode"))
	}

	sqlConn := ""
	if port != "" && host != "" {
		sqlConn = username + ":" + password + "@tcp(" + host + ":" + port + ")/" + database + "?charset=utf8" + "&parseTime=True"
	} else {
		sqlConn = username + ":" + password + "@tcp/" + database + "?charset=utf8" + "&parseTime=True"
	}
	DB, err = gorm.Open(driver, sqlConn)
	if err != nil {
		panic(err.Error())
	}
	DB.LogMode(true)
}
