package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"revel/app/models/database"
	"fmt"
	"github.com/shopspring/decimal"
)

type Import struct {
	gorm.Model
	Name           string          `json:"name"`
	Status         bool            `json:"status"`
	SupplierID     uint            `gorm:"ForeignKey:SupplierID"`
	WarehouseID    uint            `gorm:"ForeignKey:WarehouseID"`
	UpdateType     string          `json:"update_type"`
	FileName       string          `json:"filename"`
	Mapping        []Mapping       `gorm:"many2many:import_has_mapping"`
	QtyLimit       decimal.Decimal `gorm:"type:decimal(5,2)"`
	QtyModType     string
	QtyModMethod   string
	PriceModifier  decimal.Decimal `gorm:"type:decimal(5,2)"`
	PriceModType   string
	PriceModMethod string
	HeaderRow      int
}

func (m *Import) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Import) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Import) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddImport insert a new Import into database and returns
// last inserted import on success.
func (m Import) Add() (Import, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateImport update a Import into database and returns
// last nil on success.
func (m Import) Update() (Import, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()

	for i, mapping := range m.Mapping {
		fmt.Print(mapping)
		if err = tx.Where("id = ?", mapping.ID).Assign(mapping).FirstOrCreate(&mapping).Error; err != nil {
			tx.Rollback()
			return m, err
		}
		m.Mapping[i] = mapping
	}

	if err = tx.Model(&m).Association("Mapping").Replace(m.Mapping).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	if err = tx.Set("gorm:save_associations", false).Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteImport Delete Import from database and returns
// last nil on success.
func (m Import) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetImports Get all Import from database and returns
// list of Import on success
func (m Import) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Import, error) {
	var (
		err     error
		imports []Import
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&imports).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&imports).Error
	} else {
		err = tx.Find(&imports).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&imports).Error
	}

	if err != nil {
		tx.Rollback()
		return count, imports, err
	}

	tx.Commit()

	return count, imports, err
}

// GetImport Get a Import from database and returns
// a Import on success
func (m Import) GetOne(id uint64) (Import, error) {
	var (
		imports Import
		err     error
	)
	tx := database.DB.Begin()
	if err = tx.Preload("Mapping").Last(&imports, id).Error; err != nil {
		tx.Rollback()
		return imports, err
	}
	tx.Commit()
	return imports, err
}

func MigrateImport() {
	database.DB.AutoMigrate(&Import{})
}

func (Import) TableName() string {
	return "imports"
}

func (i *Import) ColToMapping(header []string) map[int]string {
	colToMapping := map[int]string{}
	if len(header) > 0 && len(i.Mapping) > 0 {
		for index, v := range header {
			for _, value := range i.Mapping {
				if v == value.Key {
					colToMapping[index] = v
				}
			}
		}
	}

	return colToMapping
}

func (i *Import) Validate(v *revel.Validation) {
	//Validation rules here
}
