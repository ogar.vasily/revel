package models

import (
	"strconv"
	"fmt"
	"github.com/jinzhu/gorm"
)

type AttrHasAttrValues struct {
	//gorm.JoinTableHandler
	ID uint `gorm:"primary_key"`
	Attribute    []Attr       `gorm:"ForeignKey:ID;AssociationForeignKey:AttrID"`
	AttrID       uint
	AttrValuesID uint
	Values       []AttrValues `gorm:"ForeignKey:ID;AssociationForeignKey:AttrValuesID"`
}

func (*AttrHasAttrValues) Add(handler gorm.JoinTableHandlerInterface, db *gorm.DB, foreignValue interface{}, associationValue interface{}) error {
	foreignPrimaryKey, _ := strconv.ParseUint(fmt.Sprint(db.NewScope(foreignValue).PrimaryKeyValue()), 10, 32)
	associationPrimaryKey, _ := strconv.ParseUint(fmt.Sprint(db.NewScope(associationValue).PrimaryKeyValue()), 10, 32)
	result := db.Unscoped().Model(&AttrHasAttrValues{}).Where(map[string]interface{}{
		"attr_id":        foreignPrimaryKey,
		"attr_values_id": associationPrimaryKey,
	}).Update(map[string]interface{}{
		"attr_id":        uint(foreignPrimaryKey),
		"attr_values_id": uint(associationPrimaryKey),
	}).RowsAffected
	if result == 0 {
		return db.Create(&AttrHasAttrValues{
			ID:           uint(1),
			AttrID:       uint(foreignPrimaryKey),
			AttrValuesID: uint(associationPrimaryKey),
		}).Error
	}

	return nil
}

func (*AttrHasAttrValues) Delete(handler gorm.JoinTableHandlerInterface, db *gorm.DB, sources ...interface{}) error {
	return db.Delete(&AttrHasAttrValues{}).Error
}

func (pa *AttrHasAttrValues) JoinWith(handler gorm.JoinTableHandlerInterface, db *gorm.DB, source interface{}) *gorm.DB {
	return db.Joins("INNER JOIN attr_has_attr_values AS T1 ON T1.attr_values_id = attr_values.id")
}
