package models

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"revel/app/models/database"
	"time"
	"github.com/shopspring/decimal"
)

type ProductAttributesValues struct {
	ID                uint            `gorm:"primary_key"`
	Products          []Product       `gorm:"ForeignKey:ProductID"`
	ProductID         uint
	Attributes        []Attr          `gorm:"ForeignKey:AttributesID"`
	AttributesID      uint
	AttributeValues   []AttrValues    `gorm:"ForeignKey:AttributeValuesID"`
	AttributeValuesID uint
	Text              string          `gorm:"type:TEXT;DEFAULT:null;"`
	Number            decimal.Decimal `gorm:"type:decimal(10,2);DEFAULT:null;"`
	VarChar           string          `gorm:"size:255;DEFAULT:null;"`
	Boolean           uint8           `gorm:"DEFAULT:null;"`
	DateTime          time.Time       `gorm:"DEFAULT:null"`
}

func (m *ProductAttributesValues) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *ProductAttributesValues) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *ProductAttributesValues) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddProduct insert a new ProductAttributesValues into database and returns
// last inserted product on success.
func (m ProductAttributesValues) Add() (ProductAttributesValues, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	fmt.Println(&m)
	if err = tx.Set("gorm:save_associations", false).Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateProduct update a ProductAttributesValues into database and returns
// last nil on success.
func (m ProductAttributesValues) Update() (ProductAttributesValues, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()

	if err = tx.Set("gorm:save_associations", false).Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteProduct Delete ProductAttributesValues from database and returns
// last nil on success.
func (m ProductAttributesValues) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetProducts Get all ProductAttributesValues from database and returns
// list of ProductAttributesValues on success
func (m ProductAttributesValues) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []ProductAttributesValues, error) {
	var (
		err      error
		products []ProductAttributesValues
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&products).Count(&count).Error
		err = tx.Preload("AttrValues").Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&products).Error
	} else {
		err = tx.Find(&products).Count(&count).Error
		err = tx.Preload("AttrValues").Order(order).Offset(offset).Limit(limit).Find(&products).Error
	}

	if err != nil {
		tx.Rollback()
		return count, products, err
	}

	tx.Commit()

	return count, products, err
}

// GetProduct Get a ProductAttributesValues from database and returns
// a ProductAttributesValues on success
func (m ProductAttributesValues) GetOne(id uint64) (ProductAttributesValues, error) {
	var (
		product ProductAttributesValues
		err     error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&product).Error; err != nil {
		tx.Rollback()
		return product, err
	}
	tx.Commit()
	return product, err
}

func (product *ProductAttributesValues) Migrate() {
	database.DB.AutoMigrate(&ProductAttributesValues{})
}

func (product *ProductAttributesValues) Validate(v *revel.Validation) {
	//Validation rules here
}
