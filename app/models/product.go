package models

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"revel/app/models/database"
)

type Product struct {
	gorm.Model
	Name                 string     `json:"name" gorm:"size:255;not null"`
	Status               uint8      `gorm:"default:1;not null"`
	Description          string     `gorm:"type:TEXT"`
	Weight               int
	Barcode              string
	Ean                  string
	Upc                  string
	Category             []Category `gorm:"many2many:product_categories;save_associations:false"`
	Attributes           []Attr     `gorm:"many2many:product_has_attributes;save_associations:false"`
	Values               []ProductAttributesValues
	Suppliers            []Supplier
	SupplierHasProducts  []SupplierHasProducts
	Warehouses           []Warehouse
	WarehouseHasProducts []WarehouseHasProducts
}

func (m *Product) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Product) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Product) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddProduct insert a new Product into database and returns
// last inserted product on success.
func (m Product) Add() (Product, error) {
	var err error
	var productAttrValues ProductAttributesValues

	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	fmt.Println(m)
	w := Product{}
	w.Ean = m.Ean

	if err = tx.Set("gorm:save_associations", false).Where(w).FirstOrCreate(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}

	for _, v := range m.Values {
		productAttrValues = v
		productAttrValues.ProductID = m.ID
		if err = tx.Set("gorm:save_associations", false).Save(&productAttrValues).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}
	tx.Commit()

	return m, err
}

func (m Product) Upsert(where Product) (Product, error) {
	var err error
	var productAttrValues ProductAttributesValues

	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	//fmt.Println(&m)
	if err = tx.Set("gorm:save_associations", false).Where(where).Assign(m).FirstOrCreate(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}

	for _, v := range m.Values {
		w := ProductAttributesValues{ProductID: m.ID, AttributesID: v.AttributesID}
		productAttrValues = v
		productAttrValues.ProductID = m.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(productAttrValues).FirstOrCreate(&productAttrValues).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}

	for _, v := range m.SupplierHasProducts {
		//if !v.Price.Equal(decimal.Zero) && !v.Qty.Equal(decimal.Zero) {
		w := SupplierHasProducts{SupplierID: v.SupplierID, ProductID: m.ID}
		v.ProductID = m.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(v).FirstOrCreate(&v).Error; err != nil {
			tx.Rollback()
			return m, err
		}
		//}
	}

	for _, v := range m.WarehouseHasProducts {
		//if !v.Price.Equal(decimal.Zero) && !v.Qty.Equal(decimal.Zero) {
		w := WarehouseHasProducts{WarehouseID: v.WarehouseID, ProductID: m.ID}
		v.ProductID = m.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(v).FirstOrCreate(&v).Error; err != nil {
			tx.Rollback()
			return m, err
		}
		//}
	}
	tx.Commit()

	return m, err
}

// UpdateProduct update a Product into database and returns
// last nil on success.
func (m Product) Update() (Product, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()

	if err = tx.Model(&m).Set("gorm:save_associations", false).Association("Category").Replace(m.Category).Error; err != nil {
		tx.Rollback()
		return m, err
	}

	for _, v := range m.Values {
		v.ProductID = m.ID
		w := ProductAttributesValues{}
		w.ProductID = m.ID
		w.AttributesID = v.AttributesID
		//if err = tx.Where(w).Last(&w).Error; err != nil {
		//	tx.Rollback()
		//	return m, err
		//}
		//
		//v.ID = w.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(v).FirstOrCreate(&v).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}

	for _, v := range m.SupplierHasProducts {
		v.ProductID = m.ID
		w := SupplierHasProducts{}
		w.ProductID = m.ID
		w.SupplierID = v.SupplierID
		//if err = tx.Where(w).Last(&w).Error; err != nil {
		//	tx.Rollback()
		//	return m, err
		//}
		//
		//v.ID = w.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(v).FirstOrCreate(&v).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}

	for _, v := range m.WarehouseHasProducts {
		v.ProductID = m.ID
		w := WarehouseHasProducts{}
		w.ProductID = m.ID
		w.WarehouseID = v.WarehouseID
		//if err = tx.Where(w).Last(&w).Error; err != nil {
		//	tx.Rollback()
		//	return m, err
		//}
		//
		//v.ID = w.ID
		if err = tx.Set("gorm:save_associations", false).Where(w).Assign(v).FirstOrCreate(&v).Error; err != nil {
			tx.Rollback()
			return m, err
		}
	}

	if err = tx.Set("gorm:save_associations", false).Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteProduct Delete Product from database and returns
// last nil on success.
func (m Product) Delete() error {
	var err error
	tx := database.DB.Begin()
	fmt.Printf("Product: %+v\n", m)
	if err = tx.Last(&m, m).Error; err != nil {
		tx.Rollback()
		return err
	}
	fmt.Printf("Product found: %+v\n", m)
	//if err = tx.Delete(&m).Error; err != nil {
	//	tx.Rollback()
	//	return err
	//}
	tx.Commit()
	return err
}

// GetProducts Get all Product from database and returns
// list of Product on success
func (m Product) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Product, error) {
	var (
		err      error
		products []Product
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Model(&products).Count(&count).Error
		err = tx.Preload("Category").Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&products).Error
	} else {
		err = tx.Model(&products).Count(&count).Error
		err = tx.Preload("Category").Preload("Attributes").Preload("Values").Order(order).Offset(offset).Limit(limit).Find(&products).Error
	}

	if err != nil {
		tx.Rollback()
		return count, products, err
	}

	tx.Commit()

	return count, products, err
}

// GetProduct Get a Product from database and returns
// a Product on success
func (m Product) GetOne(id uint64) (Product, error) {
	var (
		product Product
		err     error
	)
	tx := database.DB.Begin()
	if err = tx.Preload("Values").Preload("Category").Last(&product, id).Error; err != nil {
		tx.Rollback()
		return product, err
	}
	tx.Commit()
	return product, err
}

func (m Product) GetOneBy(w Product) (Product, error) {
	var (
		product Product
		err     error
	)
	tx := database.DB.Begin()
	if err = tx.Where(w).Last(&product).Error; err != nil {
		tx.Rollback()
		return product, err
	}
	tx.Commit()
	return product, err
}

func MigrateProduct() {
	database.DB.AutoMigrate(&Product{})
}

func (Product) TableName() string {
	return "products"
}

func (product *Product) Validate(v *revel.Validation) {
	//Validation rules here
}
