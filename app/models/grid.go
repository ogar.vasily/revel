package models

import (
	"fmt"
	"regexp"
	"strings"
)

func prepareOrder(o []string) string {
	return strings.Join(o, ", ")
}

func prepareWhere(w []map[string]string) (string, []interface{}) {
	var where string
	var args []interface{}
	for i, v := range w {
		col := ToSnakeCase(v["column"])
		val := v["value"]
		fmt.Println(v["operator"])

		switch v["operator"] {
		case "equal":
			where += col + " = ?"
		case "notequal":
			where += col + " <> ?"
		case "greaterthan":
			where += col + " > ?"
		case "lessthan":
			where += col + " < ?"
		case "greaterthanorequal":
			where += col + " >= ?"
		case "lessthanorequal":
			where += col + " <= ?"
		case "startswith":
			where += col + " LIKE ?"
			val = val + "%"
		case "endswith":
			where += col + " LIKE ?"
			val = "%" + val
		case "contains":
			where += col + " LIKE ?"
			val = "%" + val + "%"
		}

		if len(w)-1 > i {
			where += " AND "
		}
		args = append(args, val)
	}
	fmt.Println(where)
	return where, args
}

func ToSnakeCase(str string) string {
	var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
	var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}
