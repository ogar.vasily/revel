package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
)

type Attr struct {
	gorm.Model

	Name            string       `gorm:"size:255;not null"`
	Code            string       `gorm:"size:45;not null"`
	Status          uint8        `gorm:"default:1;not null"`
	IsColumn        uint8        `gorm:"default:1;not null"`
	ColumnPosition  int
	IsSystem        uint8        `gorm:"default:0;not null"`
	IsRequired      uint8        `gorm:"default:1;not null"`
	AttributeSet    AttrSet      `gorm:"ForeignKey:AttributeSetID"`
	AttributeSetID  uint
	AttributeType   AttrType     `gorm:"ForeignKey:AttributeTypeID"`
	AttributeTypeID uint
	Values          []AttrValues `gorm:"many2many:AttrHasAttrValues;AssociationForeignKey:ID;ForeignKey:ID"`
	ProductValues   []ProductAttributesValues
}

func (m *Attr) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Attr) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Attr) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddAttribute insert a new Attr into database and returns
// last inserted attribute on success.
func (m Attr) Add() (Attr, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateAttribute update a Attr into database and returns
// last nil on success.
func (m Attr) Update() (Attr, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteAttribute Delete Attr from database and returns
// last nil on success.
func (m Attr) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetAttributes Get all Attr from database and returns
// list of Attr on success
func (m Attr) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Attr, error) {
	var (
		err        error
		attributes []Attr
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&attributes).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&attributes).Error
	} else {
		err = tx.Find(&attributes).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&attributes).Error
	}

	if err != nil {
		tx.Rollback()
		return count, attributes, err
	}

	tx.Commit()

	return count, attributes, err
}

// GetAttribute Get a Attr from database and returns
// a Attr on success
func (m Attr) GetOne(id uint64) (Attr, error) {
	var (
		attribute Attr
		err       error
	)
	tx := database.DB.Begin()
	if err = tx.Preload("Values").Last(&attribute, id).Error; err != nil {
		tx.Rollback()
		return attribute, err
	}
	tx.Commit()
	return attribute, err
}

// GetAttribute Get a Attr from database and returns
// a Attr on success
func (m Attr) GetOneByCode(code string, withValues bool) (Attr, error) {
	var (
		attribute Attr
		err       error
	)
	tx := database.DB.Begin()

	if withValues {
		if err = tx.Preload("Values").Last(&attribute, code).Error; err != nil {
			tx.Rollback()
			return attribute, err
		}
	} else {
		if err = tx.Last(&attribute, "code = ?", code).Error; err != nil {
			tx.Rollback()
			return attribute, err
		}
	}

	tx.Commit()
	return attribute, err
}

func (m Attr) GetAll() ([]Attr, error) {
	var (
		err        error
		attributes []Attr
	)
	tx := database.DB.Begin()
	if err = tx.Preload("Values").Find(&attributes).Error; err != nil {
		tx.Rollback()
		return attributes, err
	}
	tx.Commit()
	return attributes, err
}

func MigrateAttribute() {
	database.DB.AutoMigrate(&Attr{})
}

func (a *Attr) Validate(v *revel.Validation) {

	v.Check(a.Name, revel.ValidRequired()).Message("Name is required.")

	v.Check(a.Code, revel.ValidRequired()).Message("Code is required.")

	v.Check(a.AttributeType, revel.ValidRequired()).Message("Type is required.")

	v.Check(a.AttributeSet, revel.ValidRequired())
}
