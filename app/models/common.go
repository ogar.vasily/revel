package models

import "time"

type CommonModel struct {
	ID        uint       `gorm:"primary_key"`
	CreatedAt time.Time  `sql:"DEFAULT:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time  `sql:"DEFAULT:CURRENT_TIMESTAMP"`
	DeletedAt *time.Time `sql:"index"`
}
