package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
	"github.com/shopspring/decimal"
)

type SupplierHasProducts struct {
	gorm.Model
	SupplierID uint            `json:"supplier_id"`
	Products   []Product       `gorm:"ForeignKey:ProductID"`
	ProductID  uint            `json:"product_id"`
	Sku        string          `json:"sku"`
	Price      decimal.Decimal `gorm:"type:decimal(5,2)";json:"price"`
	Qty        decimal.Decimal `gorm:"type:decimal(5,2)";json:"qty"`
}

func (m *SupplierHasProducts) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *SupplierHasProducts) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *SupplierHasProducts) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddSupplierHasProducts insert a new SupplierHasProducts into database and returns
// last inserted supplierhasproducts on success.
func (m SupplierHasProducts) Add() (SupplierHasProducts, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateSupplierHasProducts update a SupplierHasProducts into database and returns
// last nil on success.
func (m SupplierHasProducts) Update() (SupplierHasProducts, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteSupplierHasProducts Delete SupplierHasProducts from database and returns
// last nil on success.
func (m SupplierHasProducts) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetSupplierHasProductss Get all SupplierHasProducts from database and returns
// list of SupplierHasProducts on success
func (m SupplierHasProducts) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Product, error) {
	var (
		err      error
		products []Product
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.
			Where(q, a...).
			Where("w.supplier_id = ?", m.SupplierID).
			Joins("inner join supplier_has_products as w on w.product_id=products.id").
			Model(&products).
			Count(&count).Error

		err = tx.
			Where(q, a...).
			Order(order).
			Where("w.supplier_id = ?", m.SupplierID).
			Offset(offset).
			Limit(limit).
			Joins("inner join supplier_has_products as w on w.product_id=products.id").
			Find(&products).Error
	} else {
		err = tx.
			Where("w.supplier_id = ?", m.SupplierID).
			Joins("inner join supplier_has_products as w on w.product_id=products.id").
			Model(&products).
			Count(&count).Error

		err = tx.
			Order(order).
			Where("w.supplier_id = ?", m.SupplierID).
			Offset(offset).
			Limit(limit).
			Joins("inner join supplier_has_products as w on w.product_id=products.id").
			Find(&products).Error
	}

	if err != nil {
		tx.Rollback()
		return count, products, err
	}

	tx.Commit()

	return count, products, err
}

// GetSupplierHasProducts Get a SupplierHasProducts from database and returns
// a SupplierHasProducts on success
func (m SupplierHasProducts) GetOne(id uint64) (SupplierHasProducts, error) {
	var (
		supplierhasproducts SupplierHasProducts
		err                 error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&supplierhasproducts, id).Error; err != nil {
		tx.Rollback()
		return supplierhasproducts, err
	}
	tx.Commit()
	return supplierhasproducts, err
}

func (m SupplierHasProducts) FindByProductId(id uint64) ([]SupplierHasProducts, error) {
	var (
		supplierHasProducts []SupplierHasProducts
		err                 error
	)
	tx := database.DB.Begin()
	if err = tx.Where(&SupplierHasProducts{ProductID: uint(id)}).Find(&supplierHasProducts).Error; err != nil {
		tx.Rollback()
		return supplierHasProducts, err
	}
	tx.Commit()
	return supplierHasProducts, err
}

func MigrateSupplierHasProducts() {
	database.DB.AutoMigrate(&SupplierHasProducts{})
}

func (SupplierHasProducts) TableName() string {
	return "supplier_has_products"
}

func (supplierhasproducts *SupplierHasProducts) Validate(v *revel.Validation) {
	//Validation rules here
}
