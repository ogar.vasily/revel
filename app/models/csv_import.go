package models

//
//import (
//	"errors"
//	"fmt"
//	"github.com/jinzhu/gorm"
//	"revel/app/models/database"
//)
//
//type CsvImport struct {
//}
//
//func (m *CsvImport) BeforeCreate(scope *gorm.Scope) error {
//	// Do something before create
//	return nil
//}
//
//func (m *CsvImport) BeforeUpdate(scope *gorm.Scope) error {
//	// Do something before create
//	return nil
//}
//
//func (m *CsvImport) AfterUpdate(scope *gorm.Scope) error {
//	// Do something before create
//	return nil
//}
//
//func (m *CsvImport) Import(where, product Product, values []AttrValues) (*Product, error) {
//	var err error
//
//	if !database.DB.NewRecord(m) {
//		return nil, errors.New("primary key should be blank")
//	}
//
//	tx := database.DB.Begin()
//	fmt.Println(&product)
//	if err = tx.Set("gorm:save_associations", false).Where(where).Assign(product).FirstOrCreate(&product).Error; err != nil {
//		tx.Rollback()
//		return nil, err
//	}
//
//	attrValues, err := m.addAttributeValue(tx, values)
//	if err != nil {
//		return nil, err
//	}
//
//
//	tx.Commit()
//
//	return &product, err
//}
//
//func (m *CsvImport) UpdateSupplier(id uint, product Product) error {
//	tx := database.DB.Begin()
//	supplier := new(SupplierHasProducts)
//	tx.Unscoped().Delete(supplier, "id =?", id)
//	for _, v := range product.SupplierHasProducts {
//		if v.Price != 0 && v.Qty != 0 {
//			v.ProductID = product.ID
//			v.ID = id
//			v.Add()
//		}
//	}
//	return nil
//}
//
//func (m *CsvImport) UpdateWarehouse(id uint, product Product) error {
//	tx := database.DB.Begin()
//	warehouse := new(WarehouseHasProducts)
//	tx.Unscoped().Delete(warehouse, "id =?", id)
//	for _, v := range product.WarehouseHasProducts {
//		if v.Price != 0 && v.Qty != 0 {
//			v.ProductID = product.ID
//			v.ID = id
//			v.Add()
//		}
//	}
//	return nil
//}
//
//func (m *CsvImport) addAttributeValue(tx *gorm.DB, values []AttrValues) ([]AttrValues, error) {
//	result := []AttrValues
//	for _, v := range values {
//		err := tx.Set("gorm:save_associations", false).Create(&v).Error
//		if err != nil {
//			tx.Rollback()
//			return nil, err
//		}
//		result = append(result, v)
//	}
//	return result, nil
//}
//
//func (m *CsvImport) productValue(tx *gorm.DB, values []ProductAttributesValues, productID uint) error {
//	var productAttrValues ProductAttributesValues
//	for _, v := range values {
//		productAttrValues = v
//		productAttrValues.ProductID = productID
//		if err := tx.Set("gorm:save_associations", false).Where(v).Assign(productAttrValues).FirstOrCreate(&productAttrValues).Error; err != nil {
//			tx.Rollback()
//			return err
//		}
//	}
//	return nil
//}
