package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
)

type Supplier struct {
	gorm.Model
	Name          string `json:"name" gorm:"column:Name"`
	VatNumber     string `json:"vatnumber" gorm:"column:VatNumber"`
	CompanyNumber string `json:"companynumber" gorm:"column:CompanyNumber"`
}

func (m *Supplier) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Supplier) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *Supplier) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddSupplier insert a new Supplier into database and returns
// last inserted supplier on success.
func (m Supplier) Add() (Supplier, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateSupplier update a Supplier into database and returns
// last nil on success.
func (m Supplier) Update() (Supplier, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteSupplier Delete Supplier from database and returns
// last nil on success.
func (m Supplier) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetSuppliers Get all Supplier from database and returns
// list of Supplier on success
func (m Supplier) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Supplier, error) {
	var (
		err       error
		suppliers []Supplier
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&suppliers).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&suppliers).Error
	} else {
		err = tx.Find(&suppliers).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&suppliers).Error
	}

	if err != nil {
		tx.Rollback()
		return count, suppliers, err
	}

	tx.Commit()

	return count, suppliers, err
}

// GetSupplier Get a Supplier from database and returns
// a Supplier on success
func (m Supplier) GetOne(id uint64) (Supplier, error) {
	var (
		supplier Supplier
		err      error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&supplier, id).Error; err != nil {
		tx.Rollback()
		return supplier, err
	}
	tx.Commit()
	return supplier, err
}

func (m Supplier) GetAll() ([]Supplier, error) {
	var (
		err       error
		suppliers []Supplier
	)
	tx := database.DB.Begin()
	if err = tx.Find(&suppliers).Error; err != nil {
		tx.Rollback()
		return suppliers, err
	}
	tx.Commit()
	return suppliers, err
}

func MigrateSupplier() {
	database.DB.AutoMigrate(&Supplier{})
}

func (Supplier) TableName() string {
	return "suppliers"
}

func (supplier *Supplier) Validate(v *revel.Validation) {
	//Validation rules here
}
