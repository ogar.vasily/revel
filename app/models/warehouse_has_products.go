package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"fmt"
	"github.com/revel/revel"
	"github.com/shopspring/decimal"
)

type WarehouseHasProducts struct {
	gorm.Model
	WarehouseID uint            `json:"warehouse_id"`
	Products    []Product       `gorm:"ForeignKey:ProductID"`
	ProductID   uint            `json:"product_id"`
	Sku         string          `json:"sku"`
	Price       decimal.Decimal `gorm:"type:decimal(5,2)";json:"price"`
	Qty         decimal.Decimal `gorm:"type:decimal(5,2)";json:"qty"`
}

func (m *WarehouseHasProducts) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *WarehouseHasProducts) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *WarehouseHasProducts) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddWarehouseHasProducts insert a new WarehouseHasProducts into database and returns
// last inserted warehousehasproducts on success.
func (m WarehouseHasProducts) Add() (WarehouseHasProducts, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateWarehouseHasProducts update a WarehouseHasProducts into database and returns
// last nil on success.
func (m WarehouseHasProducts) Update() (WarehouseHasProducts, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteWarehouseHasProducts Delete WarehouseHasProducts from database and returns
// last nil on success.
func (m WarehouseHasProducts) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetwarehouseHasProducts Get all WarehouseHasProducts from database and returns
// list of WarehouseHasProducts on success
func (m WarehouseHasProducts) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []Product, error) {
	var (
		err      error
		products []Product
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	fmt.Println(m)
	if q != "" && len(a) > 0 {
		err = tx.
			Where(q, a...).
			Joins("inner join warehouse_has_products as w on w.product_id=products.id").
			Where("w.warehouse_id = ?", m.WarehouseID).
			Model(&products).
			Count(&count).Error

		err = tx.
			Where(q, a...).
			Order(order).
			Where("w.warehouse_id = ?", m.WarehouseID).
			Offset(offset).
			Limit(limit).
			Joins("inner join warehouse_has_products as w on w.product_id=products.id").
			Find(&products).Error
	} else {
		err = tx.
			Where("w.warehouse_id = ?", m.WarehouseID).
			Joins("inner join warehouse_has_products as w on w.product_id=products.id").
			Model(&products).
			Count(&count).Error

		err = tx.
			Order(order).
			Where("w.warehouse_id = ?", m.WarehouseID).
			Offset(offset).
			Limit(limit).
			Joins("inner join warehouse_has_products as w on w.product_id=products.id").
			Find(&products).Error
	}

	if err != nil {
		tx.Rollback()
		return count, products, err
	}

	tx.Commit()

	return count, products, err
}

// GetWarehouseHasProducts Get a WarehouseHasProducts from database and returns
// a WarehouseHasProducts on success
func (m WarehouseHasProducts) GetOne(id uint64) (WarehouseHasProducts, error) {
	var (
		warehousehasproducts WarehouseHasProducts
		err                  error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&warehousehasproducts, id).Error; err != nil {
		tx.Rollback()
		return warehousehasproducts, err
	}
	tx.Commit()
	return warehousehasproducts, err
}

func (m WarehouseHasProducts) FindByProductId(id uint64) ([]WarehouseHasProducts, error) {
	var (
		warehouseHasProducts []WarehouseHasProducts
		err                  error
	)
	tx := database.DB.Begin()
	if err = tx.Where(&WarehouseHasProducts{ProductID: uint(id)}).Find(&warehouseHasProducts).Error; err != nil {
		tx.Rollback()
		return warehouseHasProducts, err
	}
	tx.Commit()
	return warehouseHasProducts, err
}

func MigrateWarehouseHasProducts() {
	database.DB.AutoMigrate(&WarehouseHasProducts{})
}

func (WarehouseHasProducts) TableName() string {
	return "warehouse_has_products"
}

func (warehousehasproducts *WarehouseHasProducts) Validate(v *revel.Validation) {
	//Validation rules here
}
