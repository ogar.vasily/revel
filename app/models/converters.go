package models

import (
	"github.com/gorilla/schema"
	"reflect"
	"time"
)

func ConvertFormDate(value string) reflect.Value {
	s, _ := time.Parse("2006-01-_2", value)
	return reflect.ValueOf(s)
}

func usage() {
	decoder := schema.NewDecoder()
	decoder.RegisterConverter(time.Time{}, ConvertFormDate)
}
