package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"github.com/revel/revel"
)

type AttrType struct {
	gorm.Model
	Name        string `json:"name" gorm:"column:Name"`
	Description string `json:"description" gorm:"column:Description"`
}

func (m *AttrType) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *AttrType) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *AttrType) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddAttributeType insert a new AttrType into database and returns
// last inserted attributetype on success.
func (m AttrType) Add() (AttrType, error) {
	var err error
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateAttributeType update a AttrType into database and returns
// last nil on success.
func (m AttrType) Update() (AttrType, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteAttributeType Delete AttrType from database and returns
// last nil on success.
func (m AttrType) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetAttributeTypes Get all AttrType from database and returns
// list of AttrType on success
func (m AttrType) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []AttrType, error) {
	var (
		err            error
		attributetypes []AttrType
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&attributetypes).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&attributetypes).Error
	} else {
		err = tx.Find(&attributetypes).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&attributetypes).Error
	}

	if err != nil {
		tx.Rollback()
		return count, attributetypes, err
	}

	tx.Commit()

	return count, attributetypes, err
}

// GetAttributeType Get a AttrType from database and returns
// a AttrType on success
func (m AttrType) GetOne(id uint64) (AttrType, error) {
	var (
		attributetype AttrType
		err           error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&attributetype, id).Error; err != nil {
		tx.Rollback()
		return attributetype, err
	}
	tx.Commit()
	return attributetype, err
}

func (m AttrType) GetAll() ([]AttrType, error) {
	var (
		err   error
		types []AttrType
	)
	tx := database.DB.Begin()
	if err = tx.Find(&types).Error; err != nil {
		tx.Rollback()
		return types, err
	}
	tx.Commit()
	return types, err
}

func MigrateAttributeType() {
	database.DB.AutoMigrate(&AttrType{})
}

func (AttrType) TableName() string {
	return "attributetypes"
}

func (attributetype *AttrType) Validate(v *revel.Validation) {
	//Validation rules here
}
