package models

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type Model struct {
	ID uuid.UUID `gorm:"primary_key;type:varchar(36)"`
}

func (m *Model) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	scope.SetColumn("ID", uuid)
	return nil
}
