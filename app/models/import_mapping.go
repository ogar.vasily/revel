package models

import (
	"revel/app/models/database"
)

type Mapping struct {
	ID         uint `gorm:"primary_key"`
	Key, Value string
}

// DeleteProduct Delete Product from database and returns
// last nil on success.
func (m Mapping) Delete(id uint64) error {
	var err error
	tx := database.DB.Begin()

	if err = tx.Delete(&m, "id = ?", id).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}
