package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"revel/app/models/database"

	"fmt"
	"github.com/revel/revel"
)

type AttrSet struct {
	gorm.Model
	Name string `json:"name" gorm:"column:Name"`
}

func (m *AttrSet) BeforeCreate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *AttrSet) BeforeUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

func (m *AttrSet) AfterUpdate(scope *gorm.Scope) error {
	// Do something before create
	return nil
}

// AddAttributeSet insert a new AttrSet into database and returns
// last inserted attributeset on success.
func (m AttrSet) Add() (AttrSet, error) {
	var err error
	fmt.Println(m)
	if !database.DB.NewRecord(m) {
		return m, errors.New("primary key should be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Create(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()

	return m, err
}

// UpdateAttributeSet update a AttrSet into database and returns
// last nil on success.
func (m AttrSet) Update() (AttrSet, error) {
	var err error

	if database.DB.NewRecord(m) {
		return m, errors.New("primary key should not be blank")
	}

	tx := database.DB.Begin()
	if err = tx.Save(&m).Error; err != nil {
		tx.Rollback()
		return m, err
	}
	tx.Commit()
	return m, err
}

// DeleteAttributeSet Delete AttrSet from database and returns
// last nil on success.
func (m AttrSet) Delete() error {
	var err error
	tx := database.DB.Begin()
	if err = tx.Delete(&m).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

// GetAttributeSets Get all AttrSet from database and returns
// list of AttrSet on success
func (m AttrSet) GetMany(offset, limit int, where []map[string]string, sort []string) (int, []AttrSet, error) {
	var (
		err           error
		attributesets []AttrSet
	)

	tx := database.DB.Begin()
	order := prepareOrder(sort)
	q, a := prepareWhere(where)

	var count int
	if q != "" && len(a) > 0 {
		err = tx.Where(q, a...).Find(&attributesets).Count(&count).Error
		err = tx.Where(q, a...).Order(order).Offset(offset).Limit(limit).Find(&attributesets).Error
	} else {
		err = tx.Find(&attributesets).Count(&count).Error
		err = tx.Order(order).Offset(offset).Limit(limit).Find(&attributesets).Error
	}

	if err != nil {
		tx.Rollback()
		return count, attributesets, err
	}

	tx.Commit()
	return count, attributesets, err
}

func (m AttrSet) GetAll() ([]AttrSet, error) {
	var (
		err           error
		attributeSets []AttrSet
	)
	tx := database.DB.Begin()
	if err = tx.Find(&attributeSets).Error; err != nil {
		tx.Rollback()
		return attributeSets, err
	}
	tx.Commit()
	return attributeSets, err
}

// GetAttributeSet Get a AttrSet from database and returns
// a AttrSet on success
func (m AttrSet) GetOne(id uint64) (AttrSet, error) {
	var (
		attributeset AttrSet
		err          error
	)
	tx := database.DB.Begin()
	if err = tx.Last(&attributeset, id).Error; err != nil {
		tx.Rollback()
		return attributeset, err
	}
	tx.Commit()
	return attributeset, err
}

func MigrateAttributeSet() {
	database.DB.AutoMigrate(&AttrSet{})
}

func (AttrSet) TableName() string {
	return "attributesets"
}

func (attributeset *AttrSet) Validate(v *revel.Validation) {
	v.Check(attributeset.Name, revel.ValidRequired()).Message("Username is required.")
}
